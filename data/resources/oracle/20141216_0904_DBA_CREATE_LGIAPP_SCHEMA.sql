-- ------------------------------------------------------------------------------------------------
--  Description: Creation du sch�ma LGIAPP.               
--
--  Historique:
--  -------------  ----------  -----------------------------------------------------------------
--  Patrice S      2013-11-18  Creation
--  Patrice S      2014-12-16  Tester la pr�sence du sch�ma LGIAPP.
-- ------------------------------------------------------------------------------------------------

declare
    temp_tbs_name varchar2(30) := '';
    cr_lgiapp_user varchar2(255) :=
        'create user lgiapp'                 || chr(10) ||
        '  identified by masterlg1'          || chr(10) ||
        '  default tablespace lgiapp'        || chr(10) ||
        '  temporary tablespace ##TEMPTBS##' || chr(10) ||
        '  profile DEFAULT'                  || chr(10) ||
        '  quota unlimited on lgiapp';
    
    statement varchar2(500) := '';
    l_schema_exists number := 0;
begin

    select count(1) into l_schema_exists
      from dba_users
     where username = 'LGIAPP';

    if l_schema_exists = 0 then     
        select temporary_tablespace into temp_tbs_name 
          from dba_users
         where username = 'SYSTEM';
         
        statement := replace( cr_lgiapp_user, '##TEMPTBS##', temp_tbs_name);
        execute immediate statement; 

    else
        dbms_output.put_line('The schema LGIAPP already exists.');  
    end if;
          
end;
/

GRANT SELECT ANY TABLE TO LGIAPP;
GRANT EXECUTE ANY PROCEDURE TO LGIAPP;

GRANT SELECT ON SYS.V_$DATABASE TO LGIAPP;
GRANT SELECT ON SYS.V_$INSTANCE TO LGIAPP;
GRANT SELECT ON SYS.V_$SESSION TO LGIAPP;
GRANT SELECT ON SYS.DBA_JOBS TO LGIAPP;
GRANT SELECT ON SYS.DBA_JOBS_RUNNING TO LGIAPP;
GRANT SELECT ON SYS.DBA_DIRECTORIES TO LGIAPP;
GRANT EXECUTE ON SYS.DBMS_LOCK TO LGIAPP;
GRANT SELECT ON SYS.DBA_DB_LINKS TO LGIAPP;
GRANT SELECT ON SYS.DBA_DATA_FILES TO LGIAPP;
GRANT SELECT ON SYS.DBA_TABLESPACES TO LGIAPP;

grant create session to lgiapp;
grant create table to lgiapp;
grant create type to lgiapp;
grant create procedure to lgiapp;
grant create synonym to lgiapp;

-- ALTER USER LGIAPP ACCOUNT LOCK PASSWORD EXPIRE;

