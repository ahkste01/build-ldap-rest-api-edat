declare
  nCount NUMBER;
BEGIN
  SELECT count(*) into nCount FROM dba_tables where table_name = 'AUTHLDAPSINFO' and owner = 'LGIAPP';
  if (nCount <= 0) then
    execute immediate '
      create table LGIAPP.AUTHLDAPSINFO (   
        ldaps           NUMBER not null,
        relay_namespace VARCHAR2(30) ,
        relay_name      VARCHAR2(30) ,
        uri             VARCHAR2(255) ,
        ad_domain       VARCHAR2(255) not null
        )
        tablespace LGIAPP
        pctfree 10
        initrans 1
        maxtrans 255
        storage (
          initial 128K
          next 128K
          minextents 1
          maxextents unlimited
          pctincrease 0
        )';
        
    execute immediate '  
      alter table LGIAPP.AUTHLDAPSINFO
          add constraint AD_DOMAIN primary key (AD_DOMAIN)
          using index
          tablespace LGIAPP
          pctfree 10
          initrans 2
          maxtrans 255
          storage (
          initial 128K
          next 128K
          minextents 1
          maxextents unlimited
          pctincrease 0
          )';
  end if;
END;
/

INSERT into LGIAPP.AUTHLDAPSINFO (ldaps, relay_namespace, relay_name, uri, AD_domain) 
  select 0, 'keycloak', 'ci-dev', 'http://localhost:8081', 'logibec.com' FROM DUAL
  where not exists(select * from LGIAPP.AUTHLDAPSINFO where (AD_domain = 'logibec.com'));
commit;

CREATE OR REPLACE TYPE LgiApp.TAuthLdapsInfo IS OBJECT (

   ldaps            INTEGER,                    -- [1=LDAPS, 0=LDAP]
   relay_namespace  varchar2(40),               -- Parametre pour Keycloak Rest API
   relay_name       varchar2(30),               -- Parametre pour Keycloak Rest API
   uri              varchar2(255),              -- Parametre pour Keycloak Rest API
   currentDomain    varchar2(80),

   CONSTRUCTOR FUNCTION TAuthLdapsInfo RETURN SELF AS RESULT,
   MEMBER PROCEDURE GetConfigForDomain(pDomain in varchar2)
 );
/ 
CREATE OR REPLACE TYPE BODY LgiApp.TAuthLdapsInfo AS

   -- Contructor de la classe --
   CONSTRUCTOR FUNCTION TAuthLdapsInfo RETURN SELF AS RESULT AS
   BEGIN
     self.ldaps := 0;
     self.currentDomain := 'xxx';
     RETURN;
   END;

   MEMBER PROCEDURE GetConfigForDomain(pDomain in varchar2) IS
   BEGIN
     if (self.currentDomain <> pDomain) then
       -- SELECT 1, 'keycloak', 'ci-dev', 'http://localhost:8081' INTO ldaps, Relay_namespace, relay_name, Uri FROM DUAL;
       begin
         SELECT ldaps, relay_namespace, relay_name, uri INTO ldaps, Relay_namespace, relay_name, Uri FROM AUTHLDAPSINFO WHERE AD_DOMAIN = pDomain;
       exception
         When NO_DATA_FOUND then
           ldaps := 0;
       end;
       currentDomain := pDomain;
     end if;
   END GetConfigForDomain;
END;
/

--- grant ALL type on lgiapp.tauthldapsinfo to PKG_LDAP_OWNER;
--- PKG_LDAP_OWNER is the owner of LgiPaAuthentication
GRANT execute on lgiapp.TauthLdapsInfo to PKG_LDAP_OWNER;

GRANT execute on lgiapp.pljson to PKG_LDAP_OWNER;
GRANT execute on lgiapp.pljson_list to PKG_LDAP_OWNER;
GRANT execute on lgiapp.json_value to PKG_LDAP_OWNER;

DECLARE
  nCount NUMBER;
BEGIN
  SELECT COUNT(*) INTO nCount FROM dba_network_acls where ACL like '%lgi_acl_keycloak_api_http.xml';

  if (nCount <= 0) then
    LGIADM.PA_ACL_MANAGEMENT.create_acl('keycloak_api_http', 'localhost', 8081, 8081, 'ACL Pour service Keycloak LDAPS');
    LGIADM.PA_ACL_MANAGEMENT.grant_acl('keycloak_api_http', 'PKG_LDAP_OWNER', 'connect', true, 'acces par cb1000.LgiPaAthenticationCore');
  end if;
END;
/
