-- ------------------------------------------------------------------------------------------------
--  Description: Ce script permet de cr�er le tablepace pour le sch�ma LGIAPP.
--
--  Historique:
--  -------------  ----------  -----------------------------------------------------------------
--  Patrice S      2014-11-25  Creation
--  Patrice S      2019-06-26  Change initial, next and maxsize.
-- ------------------------------------------------------------------------------------------------    

declare

    -- OS Types
    c_os_windows              constant varchar2(7)  := 'Windows';
    c_os_unix                 constant varchar2(4)  := 'Unix';

    -- Oracle Database Releases
    c_db_v9                   constant varchar2(2)  := '9';
    c_db_v11                  constant varchar2(2)  := '11';

    -- Tablespace create for Solaris
    l_stmt_sol_cr_tbs         varchar2(1024) := 'CREATE TABLESPACE LGIAPP LOGGING DATAFILE ''/u03/oracle/@/dbfFiles/@lgiapp01.dbf'' SIZE 100M AUTOEXTEND ON NEXT 100M MAXSIZE 5120M EXTENT MANAGEMENT LOCAL AUTOALLOCATE SEGMENT SPACE MANAGEMENT AUTO';

    -- Tablespace create for Windows
    l_stmt_win_cr_tbs         varchar2(1024) := 'CREATE TABLESPACE LGIAPP LOGGING DATAFILE ''e:\oracle\@\dbfFiles\@lgiapp01.dbf'' SIZE 100M AUTOEXTEND ON NEXT 100M MAXSIZE 5120M EXTENT MANAGEMENT LOCAL AUTOALLOCATE SEGMENT SPACE MANAGEMENT AUTO' ;

    g_os_type                 varchar2(10) := '';
    g_db_version              varchar2(2)  := '';
    g_tbs_exists              number       := 0;
            
    -------------------------------------------------------------------------------------------------
    -- Function:     get_os_type
    --
    -- Description:  This function is used to retrieve the OS type on which the database is
    --               running.
    --
    -- Parameters:   None.
    --
    -- Historic:
    -- -------------  ----------  -----------------------------------------------------------------
    -- Patrice S      2010-05-21  Creation
    -------------------------------------------------------------------------------------------------
    function get_os_type
    return varchar2 is

        l_proc_name   varchar2(30) := 'get_os_type()';
        l_os_type     varchar2(10) := '';                -- Operating System on which the DB is running

    begin

        select decode( substr( member, 1, 1 ) ,  '/', c_os_unix, c_os_windows ) into l_os_type
          from v$logfile
         where rownum=1;

        return l_os_type;

    end get_os_type;

    -------------------------------------------------------------------------------------------------
    -- Function:     get_db_version
    --
    -- Description:  This function is used to retrieve the version of the database.
    --
    -- Parameters:   None.
    --
    -- Historic:
    -- -------------  ----------  -----------------------------------------------------------------
    -- Patrice S      2012-04-13  Creation
    -------------------------------------------------------------------------------------------------
    function get_db_version
    return varchar2 is

        l_proc_name   varchar2(30) := 'get_db_version()';
        l_db_version  varchar2(2) := '';                -- Database version

    begin

        select substr( version, 1, instr(version,'.',1,1)-1 ) into l_db_version
          from dba_registry
         where comp_id = 'CATALOG';

        return l_db_version;

    end get_db_version;

begin

    select count(*) into g_tbs_exists
      from dba_tablespaces
     where tablespace_name = 'LGIAPP'; 

    -- Does the LGIAPP tablespace already exist?
    if  g_tbs_exists < 1 then
        
        g_os_type := get_os_type;
        
        -- Create LGIAPP tablespace
        if  g_os_type =  c_os_unix then
    
            execute immediate l_stmt_sol_cr_tbs;
        
        else
    
            execute immediate l_stmt_win_cr_tbs;
        
        end if;
        
        dbms_output.put_line('Le tablespace LGIAPP a ete cree.');
        
    else
        dbms_output.put_line('Le tablespace LGIAPP est deja present.');
    end if;    

end;
/