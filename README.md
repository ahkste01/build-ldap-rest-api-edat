## **Instruction pour corriger et refaire le build EDAT de LDAP-REST-API**

1)	Connecter dans un serveur Solaris qui a `/u01/app/lgi/deploy`  

2)	Faire le check-out de git clone  

3)	Aller dans le répertoire `/u01/app/lgi/deploy/build-ldap-rest-api-edat/data`  

4)	Modifier le fichier `deploy.xml`  

5)	Aller dans le répertoire `/u01/app/lgi/deploy/build-ldap-rest-api-edat`  

6) 	Exécuter le script `rebuild_edat.sh`  

7)	Debug/Test, exécuter le script `deploy.run.sh`  

8)	transférer le fichier `/u01/app/lgi/deploy/build-ldap-rest-api-edat/out/deploy_ldaps-1.0.0.zip.edat dans s2consoldev1701:/u01/lgiupdate/pkg/04-infra/deploy_ldaps/`  


