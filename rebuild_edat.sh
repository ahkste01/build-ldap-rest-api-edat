#!/bin/bash 
clear
JAVA_HOME=/u01/app/lgi/jdk1.6.0_45; export JAVA_HOME
PATH=$JAVA_HOME/bin:$PATH;export PATH

PROPFILE=debug.properties

if [ -f $PROPFILE ]; then
  file_d=`grep "file.d=" $PROPFILE | awk -F'=' '{print $2}'`
  file_p=`grep "file.p=" $PROPFILE | awk -F'=' '{print $2}'`
else
  echo $PROPFILE NOT FOUND.
fi

if [ -f out/$file_d ]; then
  rm out/$file_d
fi

if [ -f out/${file_d}.edat ]; then
  rm out/${file_d}.edat
fi

/u01/app/lgi/apache-ant-1.9.7/bin/ant -f deploy.xml package -propertyfile $PROPFILE -Dmanual=true -listener org.apache.tools.ant.XmlLogger 

if [ -f out/${file_d}.edat ]; then
   cp out/${file_d}.edat /u01/app/lgi/deploy/data
else
   echo ***** out/${file_d}.edat not exists ****
fi
