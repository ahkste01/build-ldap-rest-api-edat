#!/bin/sh
clear
PROPFILE=debug.properties

if [ -f $PROPFILE ]; then
  JAVA_HOME=/u01/app/lgi/jdk1.6.0_45;export JAVA_HOME
  PATH=$JAVA_HOME/bin:$PATH;export PATH
  myDir=`pwd`
  propFile=${myDir}/$PROPFILE

  echo [${propFile}]

  export ANT_OPTS=-agentlib:jdwp=transport=dt_socket,address=8787,server=y,suspend=y
  (cd /u01/app/lgi/deploy/2.1.4/bin; /u01/app/lgi/apache-ant-1.9.7/bin/ant -d -f Gdeploy-2.1.4.xml -Dmanual=true -propertyfile ${propFile} -listener org.apache.tools.ant.XmlLogger deploy-run )
else
  echo $PROPFILE NOT FOUND...
fi
