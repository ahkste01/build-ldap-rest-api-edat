@ECHO OFF

CLS
SET JAVA_HOME=C:\Program Files\Java\jdk1.6.0_16
SET ANT_HOME=C:\Logibec\apache-ant-1.9.7
SET PATH=%PATH%;=%JAVA_HOME%\bin

SET PROPFILE=debug.properties

if exist %PROPFILE% (
	SET file_d=deploy_ldaps-1.0.0.zip
	SET file_P=logibec
	
	echo file_d %file_d%
	echo file_p %file_p%
) else (
  ECHO %PROPFILE% NOT FOUND.
)

if EXIST out\%file_d% (
  echo  out\%file_d% exists, it will be deleted
  DEL out\%file_d%
)

if EXIST out\%file_d%.edat (
  echo  out\%file_d%.edat exists, it will be deleted
  DEL out\%file_d%.edat
)

call %ANT_HOME%\bin\ant -f deploy.xml package -propertyfile %PROPFILE% -Dmanual=true -listener org.apache.tools.ant.XmlLogger 

if EXIST out\%file_d%.edat (
   copy out\%file_d%.edat ..\data
   echo *** new %file_d%.edat copied in ..\data
) ELSE (
   echo ***** out\%file_d%.edat not exists ****
)