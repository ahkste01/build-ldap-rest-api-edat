@echo off
cls
SET PROPFILE=c:\logibec\deploy\deploy_ldaps\debug.properties
SET JAVA_HOME=C:\Program Files\Java\jdk1.6.0_16
SET ANT_HOME=C:\Logibec\apache-ant-1.9.7
SET PATH=%PATH%;=%JAVA_HOME%\bin

if exist %PROPFILE% (
   cd c:\Logibec\deploy\2.1.4\bin
   %ANT_HOME%\bin\ant -f Gdeploy-2.1.4.xml -Dmanual=true -propertyfile %PROPFILE%  -listener org.apache.tools.ant.XmlLogger deploy-run
   cd c:\Logibec\deploy\deploy_ldaps
) else (
   echo %PROPFILE% NOT FOUND...
)


